#!/usr/local/bin/python3
import sys
import asyncio
def wait_for(coros):
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(coros)


