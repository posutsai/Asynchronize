#!/usr/local/bin/python3
import sys, time, asyncio, helper

async def async_test_haha():
    time.sleep(1)
    print("this is a coroutine")


def simulate_time_consuming(x, y):
    time.sleep(1)
    print("Now computing {} + {} = {}".format(x, y, x+y))


simulate_time_consuming(4, 5)

helper.wait_for(async_test_haha())

