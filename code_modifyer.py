#!/usr/local/bin/python3
import sys
import ast


# the definition of class module https://github.com/python/cpython/blob/73cbe7a01a22d02dbe1ec841e8779c775cad3d08/Include/Python-ast.h

def asynchronize_func_def(function_def):
    kwargs = {f: getattr(function_def, f) for f in function_def._fields}
    kwargs["name"] = "async_"+kwargs["name"]
    kwargs["lineno"] = function_def.lineno
    kwargs["col_offset"] = function_def.col_offset
    return ast.AsyncFunctionDef(**kwargs)

class TreeDrawer(ast.NodeVisitor):
    def __init__(self, tree):

        self.tree = tree
        self.modified_tree = tree
        self.sync2async = []
        for i, body in enumerate(tree.body):
            if isinstance(body, ast.FunctionDef):
                self.modified_tree.body[i] = asynchronize_func_def(body)
                self.sync2async.append(body.name)
                continue
            elif isinstance(body, ast.Expr):
                if isinstance(body.value.func, ast.Attribute):
                    print(body.value.func._fields)
                elif body.value.func.id in self.sync2async:
                    self.modified_tree.body[i].value.func.id = "async_"+body.value.func.id

    def exec_modified_code(self):
        import time
        exec(compile(self.modified_tree, "<string>", "exec"), {time: time})



def import_as_str(path):
    with open(path, 'r') as f:
        return f.read()

def modify_code():
    code = import_as_str("target.py")
    tree = ast.parse(code)
    tree_drawer = TreeDrawer(tree)
    tree_drawer.exec_modified_code()

if __name__ == "__main__":
    modify_code()


