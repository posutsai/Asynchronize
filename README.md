	This project is trying to impliment Jimmy Lai's talk on [PyConTW](https://www.slideshare.net/jimmy_lai/the-journey-of-asyncio-adoption-in-instagram)
1. I use code_modifyer.py to import target.py as string
2. Use python built-in module ast to parse string
3. Transfer FunctionDef module to AsyncFunctionDef module
4. Finally, use exec to execute modified Abstract Syntex Tree 

###Note:

* Still don't know how to pass modified AsyncFunctionDef object as argument to helper.wait_for to execute it
